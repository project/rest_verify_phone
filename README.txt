INTRODUCTION
------------

The REST Verify Phone module creates a custom REST endpoint 
to verify the phone number provided for the user with a code 
sent via SMS by the SMS Framework.

 * For a full description of the module, visit the project page:
   https://drupal.org/project/rest_verify_phone

 * To submit bug reports and feature suggestions, or to track changes:
   https://drupal.org/project/issues/rest_verify_phone


TODO:
-----

1. hook form submit on /verify-phone form to update our user prop of field_phone_number_verified

2. add field name for phone_number_verified to the settings, check for todo: Settings.fieldname

3. On Install verify there is a boolean field on User Entity "field_phone_number_verified" (Settings.fieldname)
    - hide from User Form, show on Display
    - Blacklist modifying this field via api



REQUIREMENTS
------------

This module requires:
 * SMS Framework (https://drupal.org/project/smsframework)

As well as these Drupal 8 Core Modules:
 * RESTful Web Services (https://www.drupal.org/docs/8/api/restful-web-services-api)
 * Serialization (https://www.drupal.org/docs/8/api/serialization-api)
 


INSTALLATION
------------
 
 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules
   for further information.

 * Enable module (admin/modules -> CUSTOM -> REST Verify Phone)



CONFIGURATION
-------------
 
 * Enable the "Verify phone number Via temp token" REST Resource (admin/config/services/rest), 
   then select the POST Method, request formats, and your Auth providers

 * We recommend turning off the "Purge Phone Numbers" in the SMS Framework Phone Number Settings 
   and handle this feature on your front-end 
   (admin/config/smsframework/phone_number/user.user)

 


MAINTAINERS
-----------

Current maintainers:
 * J.D. Gibbs (jdiza) - https://drupal.org/u/jdiza
 * Matt King (matteking) - https://drupal.org/u/matteking
 * Matt Rahr (mogollon22) - https://www.drupal.org/u/mogollon22

This project has been sponsored by:
 * University of Arizona College of Agriculture and Life Sciences, Communications and Cyber Technologies
   https://cct.cals.arizona.edu/




Please see the project page for more information
at https://www.drupal.org/project/rest_verify_phone
