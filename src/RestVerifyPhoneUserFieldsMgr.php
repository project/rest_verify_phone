<?php

namespace Drupal\rest_verify_phone;

use Drupal\Core\Entity\EntityInterface; 
use Drupal\sms\Entity\PhoneNumberVerification;


/**
 * Class RestVerifyPhoneUserFieldsMgr.
 *
 * Manages the UserStorage fields for rest_verify_phone
 */
class RestVerifyPhoneUserFieldsMgr 
{

  /**
   * used by root module (hook_form_alter)
   * 
   * @return string
   *   A message to display
   */
  public function updatePhoneVerifiedFromCodeAfterValidate($code) {
    /** @var \Drupal\sms\Provider\PhoneNumberVerificationInterface $phone_number_verification_provider */
    $phone_number_verification_provider = \Drupal::service('sms.phone_number.verification');
    $phoneVerificationEntity = $phone_number_verification_provider->getPhoneVerificationByCode($code);
    if (is_bool($phoneVerificationEntity)) {
      \Drupal::logger('rest_verify_phone')->notice('getPhoneVerificationByCode entity NOT found!'); // it'll be false if not found or already valid
      return 'getPhoneVerificationByCode entity NOT found!';
    }
    else {
      $this->updatePhoneVerificationAfterValidate($phoneVerificationEntity);
      $this->updateUserFieldsAfterValidate($phoneVerificationEntity->getEntity());
      return 'Phone number is now verified.';
    }
  }


  /**
   * Do the SMS Framework verify step - after entity has been validated
   */
  public function updatePhoneVerificationAfterValidate(PhoneNumberVerification $phone_verification) {
      $phone_verification
        ->setStatus(TRUE)
        ->setCode('')
        ->save();
  }

  /**
   * Do the User field update - after entity has been validated
   */
  public function updateUserFieldsAfterValidate(EntityInterface $entity) {
    if ($entity->getEntityTypeId() == 'user') {
      // get the user nid
      $user_id = $entity->id();
      // \Drupal::logger('rest_verify_phone')->notice('user id is: '. $user_id);
      $user = \Drupal\user\Entity\User::load($user_id);
      if ($user && $user->id()) {
        \Drupal::logger('rest_verify_phone')->notice('todo: settings.fieldname...');
        $fieldname = 'field_phone_number_verified'; // todo: settings.fieldname
        if ($user->hasField($fieldname)) { 
          $user->set($fieldname, true);
          $user->save();
        }
        else {
          \Drupal::logger('rest_verify_phone')->notice('user ! hasField = ' . $fieldname);
        }
        
      } 
      else {
        \Drupal::logger('rest_verify_phone')->notice('no user loaded? user_id = ' . $user_id);
      }
    }
    else {
      \Drupal::logger('rest_verify_phone')->notice('entity type is not "user", cannot update user phone_number_verified.  type= ' . $entity->getEntityTypeId());
    }
  }
  
}