<?php

namespace Drupal\rest_verify_phone\Plugin\rest\resource;

use Drupal\Core\Session\AccountProxyInterface;
use Drupal\rest\Plugin\ResourceBase;
use Drupal\rest\ResourceResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Psr\Log\LoggerInterface;
use Drupal\user\UserStorageInterface;
use Drupal\Core\Flood\FloodInterface;
use Drupal\sms\Provider\PhoneNumberVerificationInterface;

use Drupal\sms\Entity\PhoneNumberVerification;
use Drupal\Core\Entity\EntityInterface; 

use Drupal\rest_verify_phone\RestVerifyPhoneUserFieldsMgr;


/**
 * Provides a resource to verify user's phone with token.
 *
 * @RestResource(
 *   id = "rest_verify_phone_token",
 *   label = @Translation("Verify phone number Via temp token"),
 *   uri_paths = {
 *     "canonical" = "/rest/verify-phone",
 *     "https://www.drupal.org/link-relations/create" = "/rest/verify-phone"
 *   }
 * )
 */
class VerifyPhoneWithTempTokenRestResource extends ResourceBase {

  /**
   * Config for Requiring Phone Number be the same as the phoneNumberVerification record
   * Phone Number is not required to validate the token, 
   * but could be required and forced to be the same if this boolean is true
   *
   * @var bool 
   */
  protected $VALIDATE_PHONE_NUMBER = false;
  
  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The user storage.
   *
   * @var \Drupal\user\UserStorageInterface
   */
  protected $userStorage;

  /**
   * The flood control mechanism.
   *
   * @var \Drupal\Core\Flood\FloodInterface
   */
  protected $flood;

  /**
   * The phone number verification service.
   *
   * @var \Drupal\sms\Provider\PhoneNumberVerificationInterface
   */
  protected $phoneNumberVerification;

  /**
   * Constructs a new ActivateUserWithTempTokenRestResource Resource object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param array $serializer_formats
   *   The available serialization formats.
   * @param \Psr\Log\LoggerInterface $logger
   *   A logger instance.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   A current user instance.
   * @param \Drupal\user\UserStorageInterface $user_storage
   *   A user storage instance.
   * @param \Drupal\sms\Provider\PhoneNumberVerificationInterface $phone_number_verification
   *   A PhoneNumberVerificationInterface instance.
   * @param \Drupal\Core\Flood\FloodInterface $flood
   *   A FloodInterface instance.
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    $plugin_definition,
    array $serializer_formats,
    LoggerInterface $logger,
    AccountProxyInterface $current_user,
    UserStorageInterface $user_storage,
    PhoneNumberVerificationInterface $phone_number_verification,
    FloodInterface $flood
    ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition, $serializer_formats, $logger);
    $this->currentUser = $current_user;
    $this->userStorage = $user_storage;
    $this->logger = $logger;
    $this->flood = $flood;
    $this->phoneNumberVerification = $phone_number_verification;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->getParameter('serializer.formats'),
      $container->get('logger.factory')->get('rest_verify_phone'),
      $container->get('current_user'),
      $container->get('entity.manager')->getStorage('user'),
      $container->get('sms.phone_number.verification'),
      $container->get('flood')
    );
  }

  /**
   * Do the sms framework entity update
   */
  function verifyPhoneAfterValidated(PhoneNumberVerification $phone_verification) {
    $userFieldsMgr = new RestVerifyPhoneUserFieldsMgr();
    $userFieldsMgr->updatePhoneVerificationAfterValidate($phone_verification);
  }

  /**
   * Do the User field update
   */
  function updateUserFieldsAfterValidated(EntityInterface $entity) {
    $userFieldsMgr = new RestVerifyPhoneUserFieldsMgr();
    $userFieldsMgr->updateUserFieldsAfterValidate($entity);
  }

  /**
   * Responds Post {"phone_number":"number", "temp_token":"sms-message:verification-code"}
   * Phone Number is not required, but could be required and forced to be the same 
   * if the VALIDATE_PHONE_NUMBER boolean is true
   *
   * @throws \Symfony\Component\HttpKernel\Exception\HttpException
   *   Throws exception expected.
   */
  public function post(array $data) {
    $responce = ['message' => 'Oops! Something went wrong.'];
    $code = 400;
    // \Drupal::logger('rest_verify_phone')->notice('post to VerifyPhoneWithTempTokenRestResource...');

    if (!empty($data['temp_token'])) {
      $token = $data['temp_token'];
      
      // update the sms_verification based on https://cgit.drupalcode.org/smsframework/tree/src/Form/VerifyPhoneNumberForm.php

      // default is 3600 seconds, or 1 hour)
      $flood_window = 600;//$this->config('sms.settings')->get('flood.verify_window');// default 21600 (6 hours)
      $flood_limit = 10;//$this->config('sms.settings')->get('flood.verify_limit');

      if (!$this->flood->isAllowed('sms.verify_phone_number', $flood_limit, $flood_window)) {
        $responce = ['message' => 'There has been too many failed verification attempts. Try again later.'];
      }
      else {
        $current_time = \Drupal::time()->getRequestTime();//$this->getRequest()->server->get('REQUEST_TIME');
      
        $phone_verification = $this->phoneNumberVerification
          ->getPhoneVerificationByCode($token);

        /**
         * If we are requiring the Phone Number to be Validated, 
         * validate it if a record exists..
         */
        if ($this->VALIDATE_PHONE_NUMBER) {
          if ($phone_verification && method_exists($phone_verification, 'getPhoneNumber')) {
            if (empty($data['phone_number']) || $data['phone_number'] !== $phone_verification->getPhoneNumber()) {
              \Drupal::logger('rest_verify_phone')->notice('Phone Numbers NOT identical: '.$data['phone_number'].' !== '. $phone_verification->getPhoneNumber().' (provided ===? entity)');
              $responce = [
                'message' => 'Sorry, the Phone Number was not found - please try again.',
                'error' => 'phone_number is required and must match the phone_verification record'
              ];
            
              return new ResourceResponse($responce, $code);
            }
          } // else handled below in getStatus if
        }
        

        if ($phone_verification && method_exists($phone_verification, 'getStatus') && !$phone_verification->getStatus()) {
          $entity = $phone_verification->getEntity();
          $phone_number_settings = $this->phoneNumberVerification
            ->getPhoneNumberSettingsForEntity($entity);
          $lifetime = $phone_number_settings->getVerificationCodeLifetime() ?: 0;

          // \Drupal::logger('rest_verify_phone')->notice('$lifetime: ' . $lifetime);
          // \Drupal::logger('rest_verify_phone')->notice('here time > expire time:? ' . ($phone_verification->getCreatedTime() + $lifetime));
          if ($current_time > $phone_verification->getCreatedTime() + $lifetime) {
            $responce = ['message' => 'Verification code has expired.'];
          }
          else {
            // we're all good
            \Drupal::logger('rest_verify_phone')->notice('code is good, verify');

            // update the phoneVerificationEntity
            $this->verifyPhoneAfterValidated($phone_verification);

            // also update our user field_phone_number_verified
            $this->updateUserFieldsAfterValidated($entity);

            $responce = ['message' => 'Phone number is now verified.'];
            $code = 200;
          }
        }
        else {
          $responce = ['message' => 'Invalid verification code.'];
        }
      }
     
      $this->flood
        ->register('sms.verify_phone_number', $flood_window);

    }
    else {
      $responce = [
        'message' => 'Sorry, there was a problem - please try again.',
        'error' => 'temp_token is required'
      ];
    }

    return new ResourceResponse($responce, $code);
  }

}
