<?php

namespace Drupal\rest_verify_phone\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Defines a form that configures forms module settings.
 */
class RestVerifyPhoneModuleConfigurationForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'rest_verify_phone_admin_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'rest_verify_phone.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('rest_verify_phone.settings');

    $form['configuration'] = [
      '#type' => 'details',
      '#title' => $this->t('Rest Verify Phone Module Configuration'),
      '#open' => TRUE,
    ];
    
    $form['configuration']['rest_verify_phone_validate_phone_number'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Require Phone Number be the same as the phoneNumberVerification record'),
      '#description' => $this->t('Phone Number is not required to validate the token, but could be required and forced to be the same. Depends on your front-end implementation..'),
      '#default_value' => $config->get('validate_phone_number'),
    ];
    $form['configuration']['rest_verify_phone_enable_debug_logs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enable Debug Messages'),
      '#description' => $this->t('Enable Debug Messages to the Drupal Database Logging module (admin/reports/dblog).'),
      '#default_value' => $config->get('enable_debug_logs'),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $this->configFactory->getEditable('rest_verify_phone.settings')
      ->set('validate_phone_number', $form_state->getValue('rest_verify_phone_validate_phone_number'))
      ->set('enable_debug_logs', $form_state->getValue('rest_verify_phone_enable_debug_logs'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}